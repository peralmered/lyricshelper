# -*- coding: utf-8 -*-

# [Short description of program]

strVersion="0.51"
strProgramHeader="---[ LyricsHelper.py v"+strVersion+" ]---------------------------------"
strProgramDescription="[ Lyrics authoring tool ]"
strWindowHeader="LyricsHelper v"+strVersion

"""

ToDo
=====

Done
=====


Notes
======

"""

################################################################################################
## Debug stuff


boolAllowScreenOutput=True
if boolAllowScreenOutput: print strProgramHeader
boolDebug=True

boolExitUsingEscapeKey=False

boolUsePickleInsteadOfJson=False

## Debug stuff
################################################################################################
## Defines


# File handling
NO_ERROR=0
numErrBit=1
FILE_EXISTS_FAIL=numErrBit
numErrBit*=2
FILE_READABLE_FAIL=numErrBit
numErrBit*=2


#--------------------------------------------------------------


MSGBOX_NONE=0
MSGBOX_NO=1
MSGBOX_YES=2
MSGBOX_OK=3
MSGBOX_CANCEL=4

PROTLOCK_LOCKED=1
PROTLOCK_PROTECTED=2

PLAY_PAUSE_BUTTON_HANDLER_PLAY=1
PLAY_PAUSE_BUTTON_HANDLER_PAUSE=2


#--------------------------------------------------------------


## Defines
###############################################################################
## Layout


###############################################################
## Fonts


# Main GUI font
fntMainFace="Segoe UI"
fntMainSize=9

# Subprocess output font
fntSubProcessFace="Consolas"
fntSubProcessSize=12


## Fonts
###############################################################
## Sizes of stuffs


numPickButtonWidth=36
numSpinBoxWidth=75
numSliderWidth=150
numFileNameWidth=400


## Sizes of stuffs
###############################################################


## Layout
###############################################################################
################################################################################################
## Constants



## Constants
################################################################################################
## Globals


numRow=-1

currentLyric=None

strAudioFileFilename=""
boolAudioLoaded=False
boolAudioPlaying=False
SliderThread=None
numAudioPositionSliderLength=750
numSongPosMillis=0
numSongLengthMillis=0
oCurrentAudio=None
AudioPlayer=None
CurrentPlayPauseButtonHandler=0
strLyricsFilename=""

currentProject=None


## Globals
################################################################################################
## Functions


def CheckFileExistsAndReadable(strFile):
  numOut=0
  boolHit=False
  if os.path.exists(strFile) and os.path.isfile(strFile):
    boolHit=True
  if not boolHit:
    numOut+=FILE_EXISTS_FAIL
  boolHit=False
  if os.access(strFile, os.R_OK):
    boolHit=True
  if not boolHit:
    numOut+=FILE_READABLE_FAIL
  return numOut


#--------------------------------------------------------------


def GetBinaryFile(strFile):
  # Returns contents of binary file in array of ints, where each int represents a byte
  arrFile=[]
  byteThis=open(strFile, "rb").read()
  for b in byteThis:
    arrFile.append(ord(b))
  return arrFile


#--------------------------------------------------------------


def GetTextFile(strFile):
  # Return contents of text file in array of strings, where each string is a line in the text file
  arrFileTemp=open(strFile, "r").readlines()
  arrFile=[]
  for this in arrFileTemp:
    arrFile.append(this.decode("utf-8"))
  return arrFile


#--------------------------------------------------------------


def WriteBinaryFile(arrData, strFile):
  # Writes binary data from arrData to binary file
  # Binary data is expected as an array of ints, where each int represents a byte
  fileOut=open(strFile, "wb")
  arrByteData=bytearray(arrData)
  fileOut.write(arrByteData)
  fileOut.close()


#--------------------------------------------------------------


def WriteTextFile(arrData, strFile):
  # Writes data from arrData to textfile
  # Data is expected as an array of strings, where each string represents a line of text
  fileOut=open(strFile, "w")
  for strThis in arrData:
    thisLine=strThis+"\n"
    #thisLine=thisLine.encode("utf-8")
    #thisLine=unicode(thisLine)
    fileOut.write(thisLine.encode("utf-8"))
  fileOut.close()


#--------------------------------------------------------------


def Out(str):
  if boolAllowScreenOutput:
    sys.stdout.write(str)


#--------------------------------------------------------------


def Fatal(str):
  # Fatal errors always output
  boolAllowScreenOutput=True
  Out("FATAL ERROR: "+str+"\n")
  exit(1)


#--------------------------------------------------------------


def Exe(arrThis):
  return subprocess.check_output(arrThis)


#--------------------------------------------------------------


def ExeWithOutput(arrExe):
  process = subprocess.Popen(
      arrExe, stdout=subprocess.PIPE, stderr=subprocess.PIPE
  )
  while True:
    out = process.stdout.read(1)
    if out == '' and process.poll() != None:
      break
    if out != '':
      if out=="\n":
        sys.stdout.write("\n")
        sys.stdout.flush()
      elif out=="\r":
        sys.stdout.write("\r")
        sys.stdout.flush()
      else:
        sys.stdout.write(out)
        #sys.stdout.flush()
  sys.stdout.write("\n")
  sys.stdout.flush()


#--------------------------------------------------------------


def EndsWith(strThis, strSub):
  return strThis[-len(strSub):]==strSub


#--------------------------------------------------------------


def StrToBool(n):
  if n=="True":
    return True
  return False


#--------------------------------------------------------------


def SetWindowHeader(strText=""):
  if strText:
    strWindowHeader="LyricsHelper v"+strVersion+" - "+strText
  else:
    strWindowHeader="LyricsHelper v"+strVersion
  winOptions.setWindowTitle(strWindowHeader)


#--------------------------------------------------------------

def ExitAll():
  global thisApp
  global boolAudioPlaying
  boolAudioPlaying=False
  SliderThread.terminate()
  # Save "Recent input files" to .INI ...yeah, ok, let's just rewrite the whole damn thing
  #CreateIniFile()
  # Actually exit
  thisApp.quit()


def ExitAllWithQuery():
  valResponse=MsgBox_YesNo("About to exit", "Do you want to exit?")
  if valResponse==MSGBOX_YES:
    ExitAll()


#--------------------------------------------------------------


def MsgBox_YesNo(strHeader, strQuestion):
  global winOptions
  flags=QtGui.QMessageBox.StandardButton.Yes
  flags|=QtGui.QMessageBox.StandardButton.No
  response=QtGui.QMessageBox.question(winOptions, strHeader, strQuestion, flags)
  if response==QtGui.QMessageBox.Yes:
    return MSGBOX_YES
  if response==QtGui.QMessageBox.No:
    return MSGBOX_NO
  return MSGBOX_NONE


def MsgBox_OkCancel(strHeader, strQuestion):
  global winOptions
  flags=QtGui.QMessageBox.StandardButton.Ok
  flags|=QtGui.QMessageBox.StandardButton.Cancel
  response=QtGui.QMessageBox.question(winOptions, strHeader, strQuestion, flags)
  if response==QtGui.QMessageBox.Ok:
    return MSGBOX_OK
  if response==QtGui.QMessageBox.Cancel:
    return MSGBOX_CANCEL
  return MSGBOX_NONE


def MsgBox_InternalError(strError):
  global winOptions
  flags=QtGui.QMessageBox.StandardButton.Ok
  response=QtGui.QMessageBox.critical(winOptions, "Internal error!", strError, flags)
  return False


def MsgBox_UserError(strError):
  global winOptions
  flags=QtGui.QMessageBox.StandardButton.Ok
  response=QtGui.QMessageBox.critical(winOptions, "BOINK", strError, flags)
  return False


def MsgBox_Info(strMessage):
  global winOptions
  flags=QtGui.QMessageBox.StandardButton.Ok
  response=QtGui.QMessageBox.information(winOptions, "Good day together!", strMessage, flags)
  return False


#--------------------------------------------------------------


def OpenLyricsFile(strInFile):
  arrResult={}
  boolError=False
  numResult=CheckFileExistsAndReadable(strInFile)
  if numResult!=0:
    boolError=True
    if numResult&FILE_EXISTS_FAIL:
      #MsgBox_UserError("Can't find file \""+strInFile+"\"!")
      strError="Can't find file \""+strInFile+"\"!"
    if numResult&FILE_READABLE_FAIL:
      #MsgBox_UserError("Can't read file \""+strInFile+"\"!")
      strError="Can't read file \""+strInFile+"\"!"

  if boolError:
    arrResult["success"]=False
    arrResult["error"]=strError
    return arrResult

  arrInFile=GetTextFile(strInFile)

  strOut=""
  for this in arrInFile:
    strOut+=this
  arrResult["success"]=True
  arrResult["lyrics"]=strOut
  return arrResult


#def OpenMarkersFile(strInFile):
#  arrResult={}
#  boolError=False
#  numResult=CheckFileExistsAndReadable(strInFile)
#  if numResult!=0:
#    if numResult&FILE_READABLE_FAIL:
#      boolError=True
#      strError="Markers file \""+strInFile+"\" exists, but is unreadable!"
#
#    if boolError:
#      arrResult["success"]=False
#      arrResult["error"]=strError
#      return arrResult
#
#  arrMarkersFile=GetTextFile(strInFile)
#  #print "len(arrMarkersFile): "+str(len(arrMarkersFile))
#  if len(arrMarkersFile)<24:
#    boolError=True
#    strError="\""+strInFile+"\" is corrupt (too few lines)"
#    arrResult["success"]=False
#    arrResult["error"]=strError
#    return arrResult
#  else:
#    n=0
#    for i in range(12):
#      strMarkerName=arrMarkersFile[n].strip()
#      thisName=strMarkerName
#      #print str(i)+": "+str(thisName)
#      thisTime=int(arrMarkersFile[n+1])
#      winUI.arrEditMarkerLine[i].setText(thisName)
#      winUI.arrMarkerTime[i]=thisTime
#      strTime=GetLengthStringFromMillis(thisTime)
#      winUI.arrLbTimeF[i].setText(strTime)
#      n+=2
#
#  arrResult["success"]=True
#  return arrResult


#def OpenAndDisplayLyrics(strFilename):
#  global strLyricsFilename
#  global currentLyric
#  strLyricsFilename=strFilename
#  arrResult=OpenLyricsFile(strLyricsFilename)
#  if arrResult["success"]==False:
#    MsgBox_UserError(arrResult["error"])      
#  if arrResult["success"]:
#    currentLyric=arrResult["lyrics"]
#    winUI.txtLyrics.clear()
#    winUI.txtLyrics.insertPlainText(currentLyric)


def FileMenu_OpenLyrics():
  global strLyricsFilename
  global currentLyric
  strLyricsFilename, filemask=QtGui.QFileDialog.getOpenFileName(winOptions, "Open lyrics file", ".", "text files (*.txt)")
  if strLyricsFilename:
    # Actual lyrics
    OpenAndDisplayLyrics(strLyricsFilename)
    # Markers
    arrResult=OpenMarkersFile(strLyricsFilename+".markers")
    if arrResult["success"]==False:
      MsgBox_UserError(arrResult["error"])      


#--------------------------------------------------------------


def WriteLyricsTextFile(arrText, strFilename):
  WriteTextFile(arrText, strFilename)
#  arrMarkersFile=[]
#  for i in range(12):
#    strMarker=unicode(winUI.arrEditMarkerLine[i].text())
#    arrMarkersFile.append(strMarker)
#    arrMarkersFile.append(str(winUI.arrMarkerTime[i]))
#  WriteTextFile(arrMarkersFile, strFilename+".markers")


def SaveLyricsFile(strFilename):
  #strLyric=winUI.txtLyrics.toPlainText().encode("utf-8")
  strLyric=winUI.txtLyrics.toPlainText()
  arrLyrics=strLyric.split("\n")
  WriteLyricsTextFile(arrLyrics, strFilename)
#  if strFilename:
#    WriteLyricsTextFile(arrLyrics, strFilename)
#  else:
#    FileMenu_SaveLyricsAs()


#def FileMenu_SaveLyricsAs():
#  global strLyricsFilename
#  strLyricsFilename, filemask=QtGui.QFileDialog.getSaveFileName(winOptions, "Save lyrics file", ".", "text files (*.txt)")
#  #print "filename: "+str(strLyricsFilename)
#  if strLyricsFilename:
#    strLyric=winUI.txtLyrics.toPlainText().encode("utf-8")
#    arrLyrics=strLyric.split("\n")
#    SaveLyricsFile(arrLyrics, strLyricsFilename)


#def FileMenu_SaveLyrics():
#  global strLyricsFilename
#  #strLyricsFilename, filemask=QtGui.QFileDialog.getSaveFileName(winOptions, "Save lyrics file", ".", "text files (*.txt)")
#  #print "filename: "+str(strLyricsFilename)
#  #if strLyricsFilename:
#  strLyric=winUI.txtLyrics.toPlainText().encode("utf-8")
#  arrLyrics=strLyric.split("\n")
#  SaveLyricsFile(arrLyrics, strLyricsFilename)


#--------------------------------------------------------------


def GetMp3LengthFromFile(strInFile):
  audio=MP3(strInFile)
  mp3length=audio.info.length
  mp3length=int(mp3length*1000)
  #print "mp3length: "+str(mp3length)
  return mp3length
  

def GetOggLengthFromFile(strInFile):
  audio=OggVorbis(strInFile)
  Ogglength=audio.info.length
  Ogglength=int(Ogglength*1000)
  return Ogglength
  

def GetWavLengthFromFile(strInFile):
  global oCurrentAudio
  return int(oCurrentAudio.duration*1000)
#  with contextlib.closing(wave.open(strInFile,'r')) as f:
#    frames = f.getnframes()
#    rate = f.getframerate()
#    duration = frames / float(rate)
#    #print(duration)
#    WavLength=int(duration*1000)
#    return WavLength
  

def OpenAudioFile(strInFile):
  global oCurrentAudio
  global AudioPlayer
  arrResult={}
  strError=""
  boolError=False
  numResult=CheckFileExistsAndReadable(strInFile)
  if numResult!=0:
    boolError=True
    if numResult&FILE_EXISTS_FAIL:
      strError="Can't find file \""+strInFile+"\"!"
      #MsgBox_UserError(strError)
    if numResult&FILE_READABLE_FAIL:
      strError="Can't read file \""+strInFile+"\"!"
      #MsgBox_UserError(strError)

  if boolError:
    arrResult["success"]=False
    arrResult["error"]=strError
    return arrResult

  try:
    AudioPlayer=pyglet.media.Player()
  except:
    e=sys.exc_info()
    strError=str(e)
    boolError=True

  if boolError:
    arrResult["success"]=False
    arrResult["error"]=strError
    return arrResult

  #oCurrentAudio=pyglet.media.StaticSource(pyglet.media.load(strInFile))
  oCurrentAudio=pyglet.media.load(strInFile)
  AudioPlayer.queue(oCurrentAudio)
#  print str(oCurrentAudio)
#  print str(AudioPlayer)

  #numSongLengthMillis=GetMp3LengthFromFile(strInFile)
  #numSongLengthMillis=GetOggLengthFromFile(strInFile)
  numSongLengthMillis=GetWavLengthFromFile(strInFile)
  arrResult={}
  arrResult["success"]=True
  arrResult["song_length_millis"]=numSongLengthMillis
  return arrResult


#def LoadAndStartAudio(strFilename):
#  global oCurrentAudio
#  global AudioPlayer
#  global numSongLengthMillis
#  global numSongPosMillis
#  global boolAudioFileLoaded
#  global strAudioFileFilename
#
#  strAudioFileFilename=strFilename
#  if boolAudioPlaying:
#    AudioPlayer.pause()
#  oCurrentAudio=None
#  arrResult=OpenAudioFile(strAudioFileFilename)
#  if arrResult["success"]==False:
#    MsgBox_UserError(arrResult["error"])
#  else:
#    numSongLengthMillis=arrResult["song_length_millis"]
#    boolAudioFileLoaded=True
#    winUI.lbAudioName.setText(strAudioFileFilename)
#
#    # Update markers
#    for i in range(12):
#      winUI.arrBtnSetF[i].setEnabled(True)
#      winUI.arrBtnGoF[i].setEnabled(True)
#      strTime=GetLengthStringFromMillis(winUI.arrMarkerTime[i])
#      winUI.arrLbTimeF[i].setText(strTime)
#
#    # Update player stuff
#    winUI.btnPlayPause.setEnabled(True)
#    winUI.sldAudioPosition.setEnabled(True)
#    numSongPosMillis=0
#    winUI.lbAudioPosition.setText("00:00/"+GetLengthStringFromMillis(numSongLengthMillis))
#    winUI.lbAudioPosition.setEnabled(True)
#
#    MusicPlay()


#def OpenAudioFileAction():
#  global oCurrentAudio
#  global AudioPlayer
#  global numSongLengthMillis
#  global numSongPosMillis
#  global boolAudioFileLoaded
#  global strAudioFileFilename
##  global VLCPlayer
#  winUI.lbAudioName.setText("[ Loading... ]")
#  #strAudioFileFilename, filemask=QtGui.QFileDialog.getOpenFileName(winOptions, "Open audio file", ".", "mp3 files (*.mp3)")
#  #strAudioFileFilename, filemask=QtGui.QFileDialog.getOpenFileName(winOptions, "Open audio file", ".", "ogg vorbis files (*.ogg)")
#  strAudioFileFilename, filemask=QtGui.QFileDialog.getOpenFileName(winOptions, "Open audio file", ".", "wav files (*.wav)")
#  if not strAudioFileFilename:
#    self.lbAudioName.setText("[ No file loaded ]")
#  if strAudioFileFilename:
#    LoadAndStartAudio(strAudioFileFilename)


#--------------------------------------------------------------


def ResetProject():
  global currentProject
  global boolAudioLoaded
  ###########################################################
  ## Reset project

  boolAudioLoaded=False
  if boolAudioPlaying:
    MusicPause()
  currentProject.ResetAll()
  # Clear stuff
  SetWindowHeader()
  # Audio player
  winUI.lbAudioName.setText("[ No file loaded ]")
  winUI.btnPlayPause.setText("Play")
  winUI.btnPlayPause.setEnabled(False)
  winUI.sldAudioPosition.setValue(0)
  winUI.sldAudioPosition.setEnabled(False)
  winUI.lbAudioPosition.setText("--:--/--:--")
  winUI.lbAudioPosition.setEnabled(False)
  boolAudioLoaded=False
  # Markers
  currentProject.RenderMarkers()
  # Lyrics
  winUI.lbLyricsName.setText("[ Unsaved ]")
  winUI.txtLyrics.clear()
  winUI.txtLyrics.insertPlainText("")

  ## Reset project
  ###########################################################


#--------------------------------------------------------------


def NewProjectAction():
  global currentProject
  global boolAudioLoaded

  boolClearAll=False

  # Markers?
  boolMarkersSet=False
  for i in range(12):
    #print "marker "+str(i)
    numTime=currentProject.GetMarkerTime(i)
    strName=currentProject.GetMarkerName(i)
    #print " "*4+"*"+strName+"*"
    if numTime!=0 or strName!="":
      boolMarkersSet=True
      break
  
  # Lyrics?
  boolLyricsSet=False
  if winUI.txtLyrics.toPlainText():
    boolLyricsSet=True

  if boolMarkersSet:
    strQuestion="There's at least one marker set"
    if boolLyricsSet:
      strQuestion+=", and lyrics that might not be saved"
    strQuestion+=". Do you want to clear this project and start a blank one?"
    numChoice=MsgBox_OkCancel("New project", strQuestion)
    if numChoice==MSGBOX_OK:
      boolClearAll=True

  if not boolMarkersSet:
    if boolLyricsSet:
      strQuestion="There are lyrics that might not be saved"
      strQuestion+=". Do you want to clear this project and start a blank one?"
      numChoice=MsgBox_OkCancel("New project", strQuestion)
      if numChoice==MSGBOX_OK:
        boolClearAll=True

  if not boolMarkersSet and not boolLyricsSet:
    boolClearAll=True

#def MsgBox_OkCancel(strHeader, strQuestion):
#  global winOptions
#  flags=QtGui.QMessageBox.StandardButton.Ok
#  flags|=QtGui.QMessageBox.StandardButton.Cancel
#  response=QtGui.QMessageBox.question(winOptions, strHeader, strQuestion, flags)
#  if response==QtGui.QMessageBox.Ok:
#    return MSGBOX_OK
#  if response==QtGui.QMessageBox.Cancel:
#    return MSGBOX_CANCEL
#  return MSGBOX_NONE

  if boolClearAll:
    ResetProject()


#--------------------------------------------------------------


#def OpenProjectAction():
#  strFilename, filemask=QtGui.QFileDialog.getOpenFileName(winOptions, "Open project file", ".", "lhproj files (*.lhproj)")
#  if strFilename:
#    currentProject.LoadFromDisk(strFilename)


#--------------------------------------------------------------


def SaveProjectAction():
  strFilename=currentProject.strProjectFilename
  if strFilename:
    currentProject.SaveToDisk(strFilename)
  else:
    SaveProjectAsAction()


#--------------------------------------------------------------


def SaveProjectAsAction():
  strFilename, filemask=QtGui.QFileDialog.getSaveFileName(winOptions, "Save project file", ".", "lhproj files (*.lhproj)")
  if strFilename:
    currentProject.SaveToDisk(strFilename)


#--------------------------------------------------------------


def GetLengthStringFromMillis(numM):
  numSeconds=int(numM/1000)
  numMinutes=int(numSeconds/60)
  numSeconds-=numMinutes*60
  strTimePos=str(numMinutes).zfill(2)+":"+str(numSeconds).zfill(2)
  return strTimePos


def MusicUpdate():
  global boolAudioPlaying
  global numSongPosMillis
  global numSongLengthMillis
  global AudioPlayer
  while True:
    if boolAudioPlaying:
      #numMillis=pygame.mixer.music.get_pos()
      numMillis=int(AudioPlayer.time*1000)
      numSongPosMillis=numMillis
      # Update text "MM:SS/MM:SS"
      strTimePos=GetLengthStringFromMillis(numMillis)+"/"+GetLengthStringFromMillis(numSongLengthMillis)
      winUI.lbAudioPosition.setText(strTimePos)
      # Update slider
      numSongPos=float(numMillis)/float(numSongLengthMillis)
      numSongPos=int(numSongPos*numAudioPositionSliderLength)
      winUI.sldAudioPosition.blockSignals(True)
      winUI.sldAudioPosition.setValue(numSongPos)
      winUI.sldAudioPosition.blockSignals(False)
#      if AudioPlayer.playing==False:
#        print "boingg"
#        AudioPlayer.pause()
#        boolAudioPlaying=False
#        break
    time.sleep(0.1)


def MusicTogglePlayPause():
  if CurrentPlayPauseButtonHandler==PLAY_PAUSE_BUTTON_HANDLER_PLAY:
    MusicPause()
  elif CurrentPlayPauseButtonHandler==PLAY_PAUSE_BUTTON_HANDLER_PAUSE:
    MusicPlay()
  else:
    MusicPlay()


def MusicPlay():
  global AudioPlayer
  global boolAudioPlaying
  global numSongPosMillis
  global CurrentPlayPauseButtonHandler
  winUI.btnPlayPause.setText("Pause")

  CurrentPlayPauseButtonHandler=PLAY_PAUSE_BUTTON_HANDLER_PLAY

  try:
    winUI.btnPlayPause.clicked.disconnect()
  except RuntimeError:
    dummy=True
  winUI.btnPlayPause.clicked.connect(MusicPause)

  AudioPlayer.play()
  AudioPlayer.seek(numSongPosMillis/1000)
  boolAudioPlaying=True


def MusicPause():
  global AudioPlayer
  global boolAudioPlaying
  global CurrentPlayPauseButtonHandler
  #numSongPosMillis=AudioPlayer.time*1000
  winUI.btnPlayPause.setText("Play")

  CurrentPlayPauseButtonHandler=PLAY_PAUSE_BUTTON_HANDLER_PAUSE

  try:
    winUI.btnPlayPause.clicked.disconnect()
  except RuntimeError:
    dummy=True
  winUI.btnPlayPause.clicked.connect(MusicPlay)

  AudioPlayer.pause()
  boolAudioPlaying=False


#--------------------------------------------------------------


def AudioPosition_Update():
  global numSongLengthMillis
  global numSongPosMillis
  numPos=winUI.sldAudioPosition.value()
  numSongPos=float(numPos)/float(numAudioPositionSliderLength)
  numSongPosMillis=numSongPos*numSongLengthMillis
  AudioPlayer.seek(numSongPosMillis/1000)
  #AudioPlayer.play()


#--------------------------------------------------------------


def MarkerNameEdited(strText, numMarker):
  currentProject.SetMarkerName(numMarker, strText)


#--------------------------------------------------------------


def MarkerButtonGoAction(numMarker):
  global numSongPosMillis
  numMillis=currentProject.GetMarkerTime(numMarker)
  numSongPosMillis=numMillis/1000
  if not boolAudioPlaying:
    MusicPlay()
  AudioPlayer.seek(numSongPosMillis)


#--------------------------------------------------------------


def MarkerButtonsSetAction(a):
  numSongPos=numSongPosMillis #winUI.sldAudioPosition.value()
  currentProject.SetMarkerTime(a, numSongPos)


#--------------------------------------------------------------


################################################################################################
# Timing


numTimingClock=0


#--------------------------------------------------------------


def TimingStart():
  global numTimingClock
  numTimingClock=time.clock()


#--------------------------------------------------------------


def GetTimingEnd():
  return time.clock()-numTimingClock


#--------------------------------------------------------------


# Timing
################################################################################################
## Dbug


boolDbug=True # master switch
boolDbugToScreen=True
boolDbugToFile=True
strDbugFilename="dbug.txt"


#--------------------------------------------------------------


def DbugInit():
  # Add check for file being writable etc etc
  if os.path.exists(strDbugFilename):
      os.remove(strDbugFilename)


#--------------------------------------------------------------


def Dbug(s):
  if boolDbug:
    if boolDbugToFile:
      with open(strDbugFilename, "a") as objFile:
        objFile.write(s+"\n")
    if boolDbugToScreen:
      print s


#--------------------------------------------------------------


## Dbug
################################################################################################



## Functions
################################################################################################


if boolDebug:
  print "Importing libraries...",

import sys
reload(sys)
sys.setdefaultencoding("utf-8")

import os
import subprocess
#from PIL import Image
#from PIL import ImageColor
import argparse
import time
import copy

sys.path.insert(0, '/projekt/_tools/xia_lib')
import xia
from xia import Fatal
from xia import Warn
from xia import Notice
from xia import Out


if boolUsePickleInsteadOfJson:
  import pickle
else:
  import io
  import codecs
  import json

  
from PySide import QtCore, QtGui

import pyglet


if boolDebug:
  print "done."

DbugInit()


################################################################################################
################################################################################################
## Test code


if 1==2:
  numScreen1=1068
  numScreen2=1911
  numActiveBuffer=1
  numPassiveBuffer=2
  arrBuffer=[]
  for i in range(9):
    numFrom=i-2
    if numFrom>=0:
      strFrom=str(numFrom)
    else:
      strFrom="[blank]"
    numTo=i
    strTo=str(numTo)
    numThisBuffer=numActiveBuffer
    arrBuffer.append("buffer "+str(numThisBuffer)+": from "+strFrom+" to "+strTo)
    if numActiveBuffer==1:
      numTemp=numActiveBuffer
      numActiveBuffer=numPassiveBuffer
      numPassiveBuffer=numTemp
    else:
      numTemp=numPassiveBuffer
      numPassiveBuffer=numActiveBuffer
      numActiveBuffer=numTemp
  for thisString in arrBuffer:
    print thisString
  exit()


## Test code
################################################################################################
################################################################################################


objParser = argparse.ArgumentParser(description=strProgramDescription)

objParser.add_argument("-i", "--infile",
                       dest="infile",
                       help="file stub (with path) to load",
                       metavar="FILE",
                       required=False)
objParser.add_argument("-o", "--outfile",
                       dest="outfile",
                       help="file (with path) to save",
                       metavar="FILE")
objParser.add_argument("-d", "--dryrun",
                       dest="dryrun",
                       help="dry run (no writing to disk)",
                       action="store_const",
                       const=True,
                       required=False,
                       default=False)
objParser.add_argument("-v", "--verbose",
                       dest="verbose",
                       help="shows additional information during processing (default is off)",
                       action="store_const",
                       const=True,
                       required=False,
                       default=False)
objParser.add_argument("-q", "--quiet",
                       dest="quiet",
                       help="no output during processing, except errors (default is off)",
                       action="store_const",
                       const=True,
                       required=False,
                       default=False)


# Parameter defaults
boolInFileGiven=False
strInFile=""
boolOutFileGiven=False
strOutFile=""
boolVerboseMode=False
boolQuietMode=False
boolDryRun=False

numArgs=0
Args=vars(objParser.parse_args())

for key in Args:
  numArgs+=1
  strValue=str(Args[key])
  #print str(key)+" = "+str(strValue)
  if key=="infile" and strValue!="None":
    boolInFileGiven=True
    strInFile=strValue
  elif key=="outfile" and strValue!="None":
    boolOutFileGiven=True
    strOutFile=strValue
  elif key=="dryrun" and strValue!="None":
    boolDryRun=xia.StrToBool(strValue)
  elif key=="verbose" and strValue!="None":
    boolVerboseMode=StrToBool(strValue)
  elif key=="quiet" and strValue!="None":
    boolQuietMode=StrToBool(strValue)

# No args = print usage and exit
if numArgs==0:
  objParser.print_help()
  exit(0)



# Adjust combinations of verbose and quiet mode
if (boolVerboseMode & boolQuietMode):
  Out("Notice: Both verbose and quiet mode sent as parameters, verbose mode chosen.\n\n")
  boolQuietMode=False

# Test infile
if boolInFileGiven:
  numResult=CheckFileExistsAndReadable(strInFile)
  if numResult!=0:
    if numResult&FILE_EXISTS_FAIL:
      Fatal("Can't find file \""+strInFile+"\"!")
    if numResult&FILE_READABLE_FAIL:
      Fatal("Can't read file \""+strInFile+"\"!")

  Out("Reading input file \""+strInFile+"\"... ")
  arrInFile=GetBinaryFile(strInFile)
  Out("done.")
  if boolVerboseMode:
    Out("\n")
  else:
    Out("\r")




###############################################################################################
###############################################################################################
## Classes


###############################################################################################
## Application


class cApp(QtGui.QApplication):

  def __init__(*args):
    QtGui.QApplication.__init__(*args)


## Application
###############################################################################################
## Options window / Main window


class cWinMain(QtGui.QMainWindow):

  def __init__(self, *args, **kwargs):
    QtGui.QMainWindow.__init__(self, *args, **kwargs)

    ###########################################################
    ## Stylesheets

    self.setStyleSheet("""
    QGroupBox {
        font-weight: bold;
        border: 1px solid #bbb;
        border-radius: 2px;
        padding: 6px;
        margin-top: 0.5em;
        margin-bottom: 8px;
    }

    QGroupBox::title {
        font-weight: bold;
        subcontrol-origin: margin;
        left: 7px;
        padding: 0 3px 0 3px;
    }
    """)

    ## Stylesheets
    ###########################################################


  # Override's QMainWindow/QWidget's "close window" event
  def closeEvent(self, event):
    ExitAllWithQuery() # exits on its own if user clicks "Yes"
    event.ignore()


  # This is an override of PySide's built-in keyboard handler
  def keyPressEvent(self, event):
    global boolPcsRunning
    modifiers = QtGui.QApplication.keyboardModifiers()
    if boolExitUsingEscapeKey:
      if event.key()==QtCore.Qt.Key_Escape:
        ExitAll()
    else:
      if event.key()==QtCore.Qt.Key_Escape:
        if boolAudioLoaded:
          MusicTogglePlayPause()
    if event.key()==QtCore.Qt.Key_F1:
      numIdx=0
      if modifiers==QtCore.Qt.ControlModifier:
        MarkerButtonsSetAction(numIdx)
      else:
        MarkerButtonGoAction(numIdx)
    if event.key()==QtCore.Qt.Key_F2:
      numIdx=1
      if modifiers==QtCore.Qt.ControlModifier:
        MarkerButtonsSetAction(numIdx)
      else:
        MarkerButtonGoAction(numIdx)
    if event.key()==QtCore.Qt.Key_F3:
      numIdx=2
      if modifiers==QtCore.Qt.ControlModifier:
        MarkerButtonsSetAction(numIdx)
      else:
        MarkerButtonGoAction(numIdx)
    if event.key()==QtCore.Qt.Key_F4:
      numIdx=3
      if modifiers==QtCore.Qt.ControlModifier:
        MarkerButtonsSetAction(numIdx)
      else:
        MarkerButtonGoAction(numIdx)
    if event.key()==QtCore.Qt.Key_F5:
      numIdx=4
      if modifiers==QtCore.Qt.ControlModifier:
        MarkerButtonsSetAction(numIdx)
      else:
        MarkerButtonGoAction(numIdx)
    if event.key()==QtCore.Qt.Key_F6:
      numIdx=5
      if modifiers==QtCore.Qt.ControlModifier:
        MarkerButtonsSetAction(numIdx)
      else:
        MarkerButtonGoAction(numIdx)
    if event.key()==QtCore.Qt.Key_F7:
      numIdx=6
      if modifiers==QtCore.Qt.ControlModifier:
        MarkerButtonsSetAction(numIdx)
      else:
        MarkerButtonGoAction(numIdx)
    if event.key()==QtCore.Qt.Key_F8:
      numIdx=7
      if modifiers==QtCore.Qt.ControlModifier:
        MarkerButtonsSetAction(numIdx)
      else:
        MarkerButtonGoAction(numIdx)
    if event.key()==QtCore.Qt.Key_F9:
      numIdx=8
      if modifiers==QtCore.Qt.ControlModifier:
        MarkerButtonsSetAction(numIdx)
      else:
        MarkerButtonGoAction(numIdx)
    if event.key()==QtCore.Qt.Key_F10:
      numIdx=9
      if modifiers==QtCore.Qt.ControlModifier:
        MarkerButtonsSetAction(numIdx)
      else:
        MarkerButtonGoAction(numIdx)
    if event.key()==QtCore.Qt.Key_F11:
      numIdx=10
      if modifiers==QtCore.Qt.ControlModifier:
        MarkerButtonsSetAction(numIdx)
      else:
        MarkerButtonGoAction(numIdx)
    if event.key()==QtCore.Qt.Key_F12:
      numIdx=11
      if modifiers==QtCore.Qt.ControlModifier:
        MarkerButtonsSetAction(numIdx)
      else:
        MarkerButtonGoAction(numIdx)
#        LaunchSubProcess(0)
##    if event.key()==QtCore.Qt.Key_Q:
##      OpenImagePreviewWindow()
##    if event.key()==QtCore.Qt.Key_A:
##      CloseImagePreviewWindow()


#----------------------------------------------------------------------------------------------


class cLabelFrame():

  def __init__(self, strTitle):
    self.Shown=True
    self.numInternalRow=0
    self.box=QtGui.QGroupBox(winUI.scrollAreaWidgetContents)
    self.box.setTitle(strTitle)
    self.boxgrid=QtGui.QGridLayout(self.box)
    self.boxlayout=QtGui.QFormLayout()
    self.boxlayout.setFieldGrowthPolicy(QtGui.QFormLayout.FieldsStayAtSizeHint)
    self.boxlayout.setLabelAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)


  def render(self, numRow):
    self.boxgrid.addLayout(self.boxlayout, numRow, 0, 1, 1)
    winUI.gridMain.addWidget(self.box, numRow, 0, 1, 1)


  def show(self):
    self.box.show()
    self.Shown=True


  def hide(self):
    self.box.hide()
    self.Shown=False


  def setStatusTip(self, strStatus):
    self.box.setStatusTip(strStatus)


  def IsShown(self):
    return self.Shown


#----------------------------------------------------------------------------------------------


class cLabelFrameRow():
  
  def __init__(self, strLabel, labelframe):
    self.Shown=True
    self.arrWidgets=[]
    self.lbfrm=labelframe
    self.lbfrm.widgetbox=QtGui.QHBoxLayout()
    self.lbfrm.widgetbox.setSizeConstraint(QtGui.QLayout.SetDefaultConstraint)
    self.label=QtGui.QLabel(self.lbfrm.box)
    self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
    self.label.setText(strLabel)
    self.lbfrm.boxlayout.setWidget(self.lbfrm.numInternalRow, QtGui.QFormLayout.LabelRole, self.label)
    #print "new row="+str(self.lbfrm.numInternalRow)+", label="+str(strLabel)


  def AddWidget(self, widget):
    self.lbfrm.widgetbox.addWidget(widget)
    self.arrWidgets.append(widget)
    #print "new widget at row "+str(self.lbfrm.numInternalRow)+": "+str(widget)


  def SetLayout(self):
    self.lbfrm.boxlayout.setLayout(self.lbfrm.numInternalRow, QtGui.QFormLayout.FieldRole, self.lbfrm.widgetbox)
    #print "set layout at row="+str(self.lbfrm.numInternalRow)
    self.lbfrm.numInternalRow+=1


  def show(self):
    self.label.show()
    self.Shown=True
    for thisWidget in self.arrWidgets:
      thisWidget.show()


  def hide(self):
    self.label.hide()
    self.Shown=False
    for thisWidget in self.arrWidgets:
      thisWidget.hide()


  def setStatusTip(self, strStatus):
    self.label.setStatusTip(strStatus)
    #print "self.arrWidgets="+str(self.arrWidgets)
    for thisWidget in self.arrWidgets:
      #print " "*4+"thisWidget="+str(thisWidget)
      thisWidget.setStatusTip(strStatus)


  def IsShown(self):
    return self.Shown


#----------------------------------------------------------------------------------------------


class cWinMainUI(object):

  #############################################################################
  ## Setup


  def setupUi(self, MainWindow):
    global numRow
    ###########################################################################
    ## Setup basic layout

    MainWindow.resize(1000, 800)
    MainWindow.setWindowTitle(strWindowHeader)
    font=QtGui.QFont()
    font.setFamily(fntMainFace)
    font.setPointSize(fntMainSize)
    MainWindow.setFont(font)
    self.centralwidget=QtGui.QWidget(MainWindow)

    self.gridOuter=QtGui.QGridLayout(self.centralwidget)
    self.gridOuter.setContentsMargins(0, 0, 0, 0)

    self.gridInner=QtGui.QGridLayout()

    self.scrollArea=QtGui.QScrollArea(self.centralwidget)
    self.scrollArea.setWidgetResizable(True)

    self.scrollAreaWidgetContents=QtGui.QWidget()
    self.gridMain=QtGui.QGridLayout(self.scrollAreaWidgetContents)

    ## Setup basic layout
    ###########################################################################
#    ## Test labelframe
#
#    arrTest=[
#      {"present": "Test option 1",
#          "argvalue": "123",
#          "statustip": "Statustip 1"},
#      {"present": "Test option 2",
#          "argvalue": "abc",
#          "statustip": "Statustip 2"},
#      ]
#
#    numRow+=1
#    self.lbfrmTest=cLabelFrame("Test")
#    self.lbfrmTest.setStatusTip("Test")
#
#    ###########################################################
#    ## Row
#    self.rowTest=cLabelFrameRow("Test", self.lbfrmTest)
#
#    #self.optTest=QtGui.QComboBox(self.lbfrmTest.box)
#    self.optTest=cCombobox(self.lbfrmTest.box, arrTest)
#    #Options_FillComboBox(self.optTest, arrTest)
#    self.rowTest.AddWidget(self.optTest)
#
#    self.rowTest.SetLayout()
#    ## Row
#    ###########################################################
#
#    self.lbfrmTest.render(numRow)
#
#    ## Test labelframe
#    ###########################################################################

    ###########################################################################
    ## Audio player box


    numRow+=1
    self.lbfrmAudioPlayer=cLabelFrame("Audio player")

    ###########################################################
    ## Audio info row

    self.rowAudioInfo=cLabelFrameRow("", self.lbfrmAudioPlayer)
    self.lbAudioName = QtGui.QLabel(self.lbfrmAudioPlayer.box)
    self.lbAudioName.setText("[ No file loaded ]")
    self.rowAudioInfo.AddWidget(self.lbAudioName)

    self.rowAudioInfo.SetLayout()

    ## Audio info row
    ###########################################################
    ## Play slider

    self.rowSlider=cLabelFrameRow("", self.lbfrmAudioPlayer)

    self.btnPlayPause=QtGui.QToolButton(self.lbfrmAudioPlayer.box)
    self.btnPlayPause.setMaximumSize(QtCore.QSize(80, 16777215))
    self.btnPlayPause.setText("Play")
    self.btnPlayPause.setEnabled(False)
    self.rowSlider.AddWidget(self.btnPlayPause)

    self.sldAudioPosition=QtGui.QSlider(self.lbfrmAudioPlayer.box)
    self.sldAudioPosition.setMinimumSize(QtCore.QSize(numAudioPositionSliderLength,0))  #numSliderWidth, 0))
    self.sldAudioPosition.setMinimum(1)
    self.sldAudioPosition.setMaximum(numAudioPositionSliderLength)
    self.sldAudioPosition.setOrientation(QtCore.Qt.Horizontal)
    self.sldAudioPosition.setEnabled(False)
    self.rowSlider.AddWidget(self.sldAudioPosition)

    self.lbAudioPosition = QtGui.QLabel(self.lbfrmAudioPlayer.box)
    self.lbAudioPosition.setText("--:--/--:--")
    self.lbAudioPosition.setEnabled(False)
    self.rowSlider.AddWidget(self.lbAudioPosition)

    self.btnBrowseAudioFile=QtGui.QToolButton(self.lbfrmAudioPlayer.box)
    self.btnBrowseAudioFile.setText("...")
    self.rowSlider.AddWidget(self.btnBrowseAudioFile)

    self.rowSlider.SetLayout()

    ## Actions
    self.btnBrowseAudioFile.clicked.connect(currentProject.LoadAudio)
    #self.sldAudioPosition.valueChanged[int].connect(AudioPosition_Update)
    self.sldAudioPosition.valueChanged.connect(AudioPosition_Update)

    ## Play slider
    ###########################################################

    self.lbfrmAudioPlayer.render(numRow)


    ## Audio player box
    ###########################################################################
    ## Time markers


    numRow+=1
    self.lbfrmMarkers=cLabelFrame("Time markers")

    self.lbSpacer=QtGui.QLabel(self.lbfrmMarkers.box)
    self.lbSpacer.setText("          ")

    self.arrLbF=[]
    self.arrBtnSetF=[]
    self.arrBtnGoF=[]
    self.arrLbTimeF=[]
    self.arrMarkerTime=[]
    self.arrEditMarkerLine=[]
    
    numTime=0
    for i in range(12):
      self.arrLbF.append(QtGui.QLabel(self.lbfrmMarkers.box))
      numF=i+1
      strF=""
#      if numF<10:
#        strF=" "
      strF+="F"+str(numF)
      self.arrLbF[i].setText(strF)
      self.arrBtnSetF.append(QtGui.QToolButton(self.lbfrmMarkers.box))
      self.arrBtnSetF[i].setText("Set")
      self.arrBtnSetF[i].setEnabled(False)
      self.arrBtnSetF[i].clicked.connect(lambda a=i:MarkerButtonsSetAction(a))
      self.arrLbTimeF.append(QtGui.QLabel(self.lbfrmMarkers.box))
      self.arrLbTimeF[i].setText("00:00")
      self.arrMarkerTime.append(numTime)
      self.arrEditMarkerLine.append(QtGui.QLineEdit(self.lbfrmMarkers.box))
      self.arrEditMarkerLine[i].setText("")
      self.arrEditMarkerLine[i].textEdited.connect(lambda a=i, b=i:MarkerNameEdited(a, b))
      self.arrBtnGoF.append(QtGui.QToolButton(self.lbfrmMarkers.box))
      self.arrBtnGoF[i].setText("Go")
      self.arrBtnGoF[i].setEnabled(False)
      self.arrBtnGoF[i].clicked.connect(lambda a=i:MarkerButtonGoAction(a))

#  F1   F4   F7   F10
#  F2   F5   F8   F11
#  F3   F6   F9   F12
    self.arrMarkerRows=[]
    n=0
    for i in range(3):
      self.arrMarkerRows.append(cLabelFrameRow("", self.lbfrmMarkers))
      for j in range(4):
        #print n,
        if j!=0:
          self.arrMarkerRows[i].AddWidget(self.lbSpacer)
        self.arrMarkerRows[i].AddWidget(self.arrLbF[n])
        self.arrMarkerRows[i].AddWidget(self.arrBtnSetF[n])
        self.arrMarkerRows[i].AddWidget(self.arrLbTimeF[n])
        self.arrMarkerRows[i].AddWidget(self.arrEditMarkerLine[n])
        self.arrMarkerRows[i].AddWidget(self.arrBtnGoF[n])
        n+=3
      #print 
      n-=(8+3)
      self.arrMarkerRows[i].SetLayout()

    self.lbfrmMarkers.render(numRow)


    ## Time markers
    ###########################################################################
    ## Lyrics box

    numRow+=1

    ###########################################################
    ## Lyrics info row

#    self.rowLyricsInfo=cLabelFrameRow("", self.scrollAreaWidgetContents)
#    self.lbLyricsName = QtGui.QLabel(self.scrollAreaWidgetContents)
#    self.lbLyricsName .setText("[ unsaved ]")
#    self.rowLyricsInfo.AddWidget(self.lbLyricsName )
#
#    self.rowLyricsInfo.SetLayout()

    ## Lyrics info row
    ###########################################################

    self.lbfrmLyrics=QtGui.QGroupBox(self.scrollAreaWidgetContents)
    self.lbfrmLyrics.setTitle("Lyrics")

    self.layoutLyricsOuter=QtGui.QVBoxLayout(self.lbfrmLyrics)

    self.lbLyricsName=QtGui.QLabel(self.scrollAreaWidgetContents)
    self.lbLyricsName.setText("[ Unsaved ]")

    self.txtLyrics=QtGui.QPlainTextEdit(self.lbfrmLyrics)
    self.txtLyrics.setReadOnly(False)
    self.txtLyrics.setMinimumSize(QtCore.QSize(500, 120))
    font=QtGui.QFont()
    font.setFamily(fntSubProcessFace)
    font.setPointSize(fntSubProcessSize)
    self.txtLyrics.setFont(font)
    self.txtLyrics.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
    self.txtLyrics.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

    self.layoutLyrics=QtGui.QGridLayout()
    self.layoutLyricsOuter.addWidget(self.lbLyricsName, 0, 0)
    self.layoutLyrics.addWidget(self.txtLyrics, 0, 0, 0, 0)

    self.layoutLyricsOuter.addLayout(self.layoutLyrics)
    self.gridMain.addWidget(self.lbfrmLyrics, numRow, 0, 1, 1)

    ## Lyrics box
    ###########################################################################
    ## Finalize layout


    self.scrollArea.setWidget(self.scrollAreaWidgetContents)
    self.gridInner.addWidget(self.scrollArea, 0, 0, 1, 1)
    self.gridOuter.addLayout(self.gridInner, 0, 0, 1, 1)
    MainWindow.setCentralWidget(self.centralwidget)


    ## Finalize layout
    ###########################################################################
    ## Menu

    self.menubar=QtGui.QMenuBar(MainWindow)
    self.menubar.setGeometry(QtCore.QRect(0, 0, 688, 21))

    self.menuFile=QtGui.QMenu(self.menubar)
    self.menuFile.setTitle("&File")

    MainWindow.setMenuBar(self.menubar)
    self.statusbar=QtGui.QStatusBar(MainWindow)
    MainWindow.setStatusBar(self.statusbar)

    self.actionNewProject=QtGui.QAction(MainWindow)
    self.actionNewProject.setText("&New project")
    self.actionNewProject.setShortcut("Ctrl+N")
    self.menuFile.addAction(self.actionNewProject)

    self.actionOpenProject=QtGui.QAction(MainWindow)
    self.actionOpenProject.setText("&Open project")
    self.actionOpenProject.setShortcut("Ctrl+O")
    self.menuFile.addAction(self.actionOpenProject)

    self.actionSaveProject=QtGui.QAction(MainWindow)
    self.actionSaveProject.setText("&Save project")
    self.actionSaveProject.setShortcut("Ctrl+S")
    self.menuFile.addAction(self.actionSaveProject)

    self.actionSaveProjectAs=QtGui.QAction(MainWindow)
    self.actionSaveProjectAs.setText("Save project &as...")
    self.actionSaveProjectAs.setShortcut("Ctrl+Shift+S")
    self.menuFile.addAction(self.actionSaveProjectAs)

    self.menuFile.addSeparator()

    self.actionOpenAudio=QtGui.QAction(MainWindow)
    self.actionOpenAudio.setText("Open &audio file")
    self.menuFile.addAction(self.actionOpenAudio)

    self.menuFile.addSeparator()

    self.actionOpenLyrics=QtGui.QAction(MainWindow)
    self.actionOpenLyrics.setText("Open &lyrics file")
    self.menuFile.addAction(self.actionOpenLyrics)

    self.actionSaveLyrics=QtGui.QAction(MainWindow)
    self.actionSaveLyrics.setText("Save l&yrics file")
    self.menuFile.addAction(self.actionSaveLyrics)

    self.actionSaveLyricsAs=QtGui.QAction(MainWindow)
    self.actionSaveLyricsAs.setText("Save &lyrics file as...")
    self.menuFile.addAction(self.actionSaveLyricsAs)

    self.menuFile.addSeparator()

    self.actionExit=QtGui.QAction(MainWindow)
    self.actionExit.setText("&Exit")
    self.actionExit.setShortcut("Ctrl+E")

    self.menuFile.addAction(self.actionExit)
    self.menubar.addAction(self.menuFile.menuAction())

    self.actionNewProject.triggered.connect(NewProjectAction)
    self.actionOpenProject.triggered.connect(currentProject.LoadFromDisk)
    self.actionSaveProject.triggered.connect(SaveProjectAction)
    self.actionSaveProjectAs.triggered.connect(SaveProjectAsAction)

    self.actionOpenAudio.triggered.connect(currentProject.LoadAudio)
    self.actionOpenLyrics.triggered.connect(currentProject.LoadLyrics)
    self.actionSaveLyrics.triggered.connect(currentProject.SaveLyrics)
    self.actionSaveLyricsAs.triggered.connect(currentProject.SaveLyricsAs)
    self.actionExit.triggered.connect(ExitAllWithQuery)

    ## Menu
    ###########################################################################


  ## Setup
  #############################################################################


## Options window / Main window
###############################################################################################
## Widgets


class cCombobox(QtGui.QComboBox):

  def __init__(self, parent, arrOpts, boolDebug=False):
    QtGui.QComboBox.__init__(self, parent)
    self.boolDebug=boolDebug
    self.arrContents=arrOpts
    if self.boolDebug:
      DBug(self.arrContents)
    self.Fill(self.arrContents)
    #self.activated[str].connect(self.UpdateStatusTip)
    self.currentIndexChanged[int].connect(self.UpdateStatusTip)
    self.UpdateStatusTip()


  #----------------------------------------------------------------------------


  def Fill(self, arrOpts):
    self.arrContents=arrOpts
    numId=0
    self.clear()
    for thisOpt in self.arrContents:
      self.addItem("")
      self.setItemText(numId, thisOpt["present"])
      numId+=1
    self.UpdateStatusTip()


  #----------------------------------------------------------------------------


  def FillWithoutClearing(self, arrOpts):
    numId=0
    for thisOpt in arrOpts:
      self.addItem("")
      self.setItemText(numId, thisOpt["present"])
      numId+=1


  #----------------------------------------------------------------------------


  def UpdateStatusTip(self):
    numIndex=self.currentIndex()
    thisContentRow=self.arrContents[numIndex]
    if "statustip" in thisContentRow:
      strTip=thisContentRow["statustip"]
    self.setStatusTip(strTip)
    if self.boolDebug:
      print "Index="+str(numIndex)+", statustip=\""+strTip+"\""


  #----------------------------------------------------------------------------


  def GetCurrentIndex(self):
    return self.currentIndex()


  #----------------------------------------------------------------------------


  def GetCurrentPresVal(self):
    numIndex=self.currentIndex()
    thisContentRow=self.arrContents[numIndex]
    return thisContentRow["present"]


  #----------------------------------------------------------------------------


  def GetCurrentArgVal(self):
    numIndex=self.currentIndex()
    thisContentRow=self.arrContents[numIndex]
    if "argvalue" in thisContentRow:
      return thisContentRow["argvalue"]
    else:
      return ""


## Widgets
###############################################################################################
## Project


class cProject():
  
  def __init__(self):
    self.ResetAll()


  #----------------------------------------------------------------------------


  def ResetAll(self):
    self.strProjectFilename=""
    self.strLyricsFilename=""
    self.strAudioFilename=""
    self.strLyrics=""
    self.arrMarkerTime=[]
    self.arrMarkerName=[]

    # Reset markers
    for i in range(12):
      self.arrMarkerTime.append(0)
      self.arrMarkerName.append("")


  #----------------------------------------------------------------------------


  def GetMarkerTime(self, numMarker):
    return self.arrMarkerTime[numMarker]


  #----------------------------------------------------------------------------


  def SetMarkerTime(self, numMarker, numTime):
    self.arrMarkerTime[numMarker]=numTime
    winUI.arrMarkerTime[numMarker]=numTime
    strTime=GetLengthStringFromMillis(numTime)
    winUI.arrLbTimeF[numMarker].setText(strTime)


  #----------------------------------------------------------------------------


  def GetMarkerName(self, numMarker):
    return self.arrMarkerName[numMarker]


  #----------------------------------------------------------------------------


  def SetMarkerName(self, numMarker, strName):
    self.arrMarkerName[numMarker]=strName
    winUI.arrEditMarkerLine[numMarker].setText(unicode(strName))


  #----------------------------------------------------------------------------


  def ReadMarkersFromGui(self):
    for i in range(12):
      self.SetMarkerTime(i, winUI.arrMarkerTime[i])
      self.SetMarkerName(i, winUI.arrEditMarkerLine[i].text())


  #----------------------------------------------------------------------------


  def RenderMarkers(self):
    for i in range(12):
      #print str(i)+":"
      thisTime=self.arrMarkerTime[i]
      winUI.arrMarkerTime[i]=thisTime
      strTime=GetLengthStringFromMillis(thisTime)
      #print "    time: "+strTime
      #winUI.arrLbTimeF[i].setText(strTime)
      self.SetMarkerTime(i, self.arrMarkerTime[i])
      #winUI.arrEditMarkerLine[i].setText(self.arrMarkerName[i])
      self.SetMarkerName(i, self.arrMarkerName[i])
      #print "    name: "+self.arrMarkerName[i]
      if boolAudioLoaded:
        winUI.arrBtnSetF[i].setEnabled(True)
        winUI.arrBtnGoF[i].setEnabled(True)
      else:
        winUI.arrBtnSetF[i].setEnabled(False)
        winUI.arrBtnGoF[i].setEnabled(False)


  #----------------------------------------------------------------------------


  def GetLyrics(self):
    return self.strLyrics


  #----------------------------------------------------------------------------


  def SetLyrics(self, strLyrics):
    self.strLyrics=strLyrics


  #----------------------------------------------------------------------------


  def RenderLyrics(self):
    winUI.txtLyrics.clear()
    winUI.txtLyrics.insertPlainText(self.strLyrics)


  #----------------------------------------------------------------------------


  def GetLyricsFilename(self):
    return self.strLyricsFilename


  #----------------------------------------------------------------------------


  def GetLyricsFilenameNoPath(self):
    return os.path.basename(os.path.abspath(self.strLyricsFilename))


  #----------------------------------------------------------------------------


  def SetLyricsFilename(self, strFilename):
    self.strLyricsFilename=strFilename


  #----------------------------------------------------------------------------


  def LoadLyrics(self, strFilename=""):
    if not strFilename:
      strFilename, filemask=QtGui.QFileDialog.getOpenFileName(winOptions, "Open lyrics file", ".", "text files (*.txt)")
    if strFilename:
      self.SetLyricsFilename(strFilename)

      boolError=False
      numResult=CheckFileExistsAndReadable(strFilename)
      if numResult!=0:
        boolError=True
        if numResult&FILE_EXISTS_FAIL:
          #MsgBox_UserError("Can't find file \""+strInFile+"\"!")
          strError="Can't find file \""+strInFile+"\"!"
        if numResult&FILE_READABLE_FAIL:
          #MsgBox_UserError("Can't read file \""+strInFile+"\"!")
          strError="Can't read file \""+strInFile+"\"!"

      if boolError:
        MsgBox_UserError(strError)
      else:
        arrInFile=GetTextFile(strFilename)

        strOut=""
        for this in arrInFile:
          strOut+=this
        self.SetLyrics(strOut)
        self.RenderLyrics()
        winUI.lbLyricsName.setText(self.GetLyricsFilenameNoPath())


  #----------------------------------------------------------------------------


  def SaveLyrics(self):
    strLyricsFile=self.GetLyricsFilename()
    if not strLyricsFile:
      self.SaveLyricsAs()
    else:
      SaveLyricsFile(strLyricsFile)
    self.SetLyricsFilename(strLyricsFile)
    winUI.lbLyricsName.setText(self.GetLyricsFilenameNoPath())


  #----------------------------------------------------------------------------


  def SaveLyricsAs(self):
    strFilename, filemask=QtGui.QFileDialog.getSaveFileName(winOptions, "Save lyrics file", ".", "text files (*.txt)")
    if strFilename:
      self.SetLyricsFilename(strFilename)
      SaveLyricsFile(strFilename)
      winUI.lbLyricsName.setText(self.GetLyricsFilenameNoPath())


  #----------------------------------------------------------------------------

  def LoadAudioWithoutPlaying(self, strFilename=""):
    global oCurrentAudio
    global AudioPlayer
    global numSongLengthMillis
    global numSongPosMillis
    global boolAudioLoaded

    #print "strFilename: "+str(strFilename)
    winUI.lbAudioName.setText("[ Loading... ]")
    if not strFilename:
      #strAudioFileFilename, filemask=QtGui.QFileDialog.getOpenFileName(winOptions, "Open audio file", ".", "mp3 files (*.mp3)")
      #strAudioFileFilename, filemask=QtGui.QFileDialog.getOpenFileName(winOptions, "Open audio file", ".", "ogg vorbis files (*.ogg)")
      strFilename, filemask=QtGui.QFileDialog.getOpenFileName(winOptions, "Open audio file", ".", "wav files (*.wav)")
      if not strFilename:
        winUI.lbAudioName.setText("[ No file loaded ]")
        return

#    if strFilename:
#      self.strAudioFilename=strFilename
#      if boolAudioPlaying:
#        AudioPlayer.pause()
#      self.oCurrentAudio=None
#
#      arrResult={}
#      strError=""
#      boolError=False
#      numResult=CheckFileExistsAndReadable(strFilename)
#      if numResult!=0:
#        boolError=True
#        if numResult&FILE_EXISTS_FAIL:
#          strError="Can't find file \""+strInFile+"\"!"
#          #MsgBox_UserError(strError)
#        if numResult&FILE_READABLE_FAIL:
#          strError="Can't read file \""+strInFile+"\"!"
#          #MsgBox_UserError(strError)
#
#      if boolError:
#        winUI.lbAudioName.setText("[ No file loaded ]")
#        MsgBox_UserError(strError)
#      else:
#        try:
#          AudioPlayer=pyglet.media.Player()
#        except:
#          e=sys.exc_info()
#          strError="Error initializing Pyglet's media player: \""+str(e)+"\""
#          boolError=True
#
#        if boolError:
#          MsgBox_UserError(strError)
#        else:
#          #oCurrentAudio=pyglet.media.StaticSource(pyglet.media.load(strInFile))
#          try:
#            oCurrentAudio=pyglet.media.load(strFilename)
#          except:
#            e=sys.exc_info()
#            strError="Error during Pyglet's loader: \""+str(e)+"\""
#            boolError=True
#
#          if boolError:
#            MsgBox_UserError(strError)
#          else:
#            # SetWindowHeader(os.path.relpath(self.strProjectFilename))
#            #winUI.lbAudioName.setText(self.strAudioFilename)
#            #strProjPath=os.path.dirname(os.path.abspath(self.strProjectFilename))
#            strAudioFileNoPath=os.path.basename(os.path.abspath(self.strAudioFilename))
#            winUI.lbAudioName.setText(strAudioFileNoPath)
#            AudioPlayer.queue(oCurrentAudio)
#
#            #numSongLengthMillis=GetMp3LengthFromFile(strInFile)
#            #numSongLengthMillis=GetOggLengthFromFile(strInFile)
#            numSongLengthMillis=GetWavLengthFromFile(strFilename)
#            boolAudioLoaded=True
#
#            self.RenderMarkers()
#
#            # Update player stuff
#            winUI.btnPlayPause.setEnabled(True)
#            winUI.sldAudioPosition.setEnabled(True)
#            numSongPosMillis=0
#            winUI.lbAudioPosition.setText("00:00/"+GetLengthStringFromMillis(numSongLengthMillis))
#            winUI.lbAudioPosition.setEnabled(True)

    self.strAudioFilename=strFilename
    if boolAudioPlaying:
      AudioPlayer.pause()
    self.oCurrentAudio=None

    arrResult={}
    strError=""
    boolError=False
    numResult=CheckFileExistsAndReadable(strFilename)
    if numResult!=0:
      boolError=True
      if numResult&FILE_EXISTS_FAIL:
        strError="Can't find file \""+strInFile+"\"!"
        #MsgBox_UserError(strError)
      if numResult&FILE_READABLE_FAIL:
        strError="Can't read file \""+strInFile+"\"!"
        #MsgBox_UserError(strError)

    if boolError:
      winUI.lbAudioName.setText("[ No file loaded ]")
      MsgBox_UserError(strError)
    else:
      try:
        AudioPlayer=pyglet.media.Player()
      except:
        e=sys.exc_info()
        strError="Error initializing Pyglet's media player: \""+str(e)+"\""
        boolError=True

      if boolError:
        MsgBox_UserError(strError)
      else:
        #oCurrentAudio=pyglet.media.StaticSource(pyglet.media.load(strInFile))
        try:
          oCurrentAudio=pyglet.media.load(strFilename)
        except:
          e=sys.exc_info()
          strError="Error during Pyglet's loader: \""+str(e)+"\""
          boolError=True

        if boolError:
          MsgBox_UserError(strError)
        else:
          # SetWindowHeader(os.path.relpath(self.strProjectFilename))
          #winUI.lbAudioName.setText(self.strAudioFilename)
          #strProjPath=os.path.dirname(os.path.abspath(self.strProjectFilename))
          strAudioFileNoPath=os.path.basename(os.path.abspath(self.strAudioFilename))
          winUI.lbAudioName.setText(strAudioFileNoPath)
          AudioPlayer.queue(oCurrentAudio)

          #numSongLengthMillis=GetMp3LengthFromFile(strInFile)
          #numSongLengthMillis=GetOggLengthFromFile(strInFile)
          numSongLengthMillis=GetWavLengthFromFile(strFilename)
          boolAudioLoaded=True

          self.RenderMarkers()

          # Update player stuff
          winUI.btnPlayPause.setEnabled(True)
          winUI.sldAudioPosition.setEnabled(True)
          numSongPosMillis=0
          winUI.lbAudioPosition.setText("00:00/"+GetLengthStringFromMillis(numSongLengthMillis))
          winUI.lbAudioPosition.setEnabled(True)


  #----------------------------------------------------------------------------


  def LoadAudio(self):
    self.LoadAudioWithoutPlaying()
    MusicPlay()


  #----------------------------------------------------------------------------


  def LoadFromDisk(self):
    strFilename, filemask=QtGui.QFileDialog.getOpenFileName(winOptions, "Open project file", ".", "lhproj files (*.lhproj)")
    if strFilename:
      self.strProjectFilename=strFilename
      boolError=False
      try:
        #arrJSON=GetTextFile(self.strProjectFilename)
        #objInputFile=file(self.strProjectFilename, "r")
        #arrJSON=json.loads(objInputFile.read().decode("utf-8"))
        #arrJSON=json.loads(objInputFile.read())

#        with open(self.strProjectFilename, encoding="utf-8") as oFile:
#          arrJSON=json.load(oFile)

#        objInputFile=file(self.strProjectFilename, "r")
#        arrJSON=json.loads(objInputFile.read().decode("utf-8"))

#        with io.open(self.strProjectFilename, encoding='utf-8') as oFile:
#          strJSON=oFile.read()

        if boolUsePickleInsteadOfJson:
          oInputFile=open(self.strProjectFilename, "rb")
          oPickledProject=pickle.load(oInputFile)
          oInputFile.close()
        else:
          with io.open(self.strProjectFilename, encoding='utf-8') as oFile:
            strJSON=oFile.read()


      except:
        e=sys.exc_info()
        strError=str(e)
        boolError=True
        strError="Failure opening project file \""+self.strProjectFilename+"\": "+strError

      if boolError:
        MsgBox_UserError(strError)
      else:

        if boolUsePickleInsteadOfJson:
          arrJSON=oPickledProject
        else:
          #print strJSON
          arrJSON=json.loads(strJSON)

        #print arrJSON["markers"]["marker_name"][0]

        arrResult={}
        arrResult["success"]=True
        arrResult["has_audio_file"]=False
        arrResult["has_lyrics_file"]=False
        arrResult["has_markers"]=False

        strAudioFile=""
        if "audio_file" in arrJSON.keys():
          self.strAudioFilename=arrJSON["audio_file"]
          arrResult["has_audio_file"]=True
          arrResult["audio_file"]=self.strAudioFilename
        strLyricsFile=""
        if "lyrics_file" in arrJSON.keys():
          self.SetLyricsFilename(arrJSON["lyrics_file"])
          winUI.lbLyricsName.setText(self.GetLyricsFilenameNoPath())
          arrResult["has_lyrics_file"]=True
          arrResult["lyrics_file"]=self.GetLyricsFilename()
        arrMarkers=[]
        if "markers" in arrJSON.keys():
          arrMarkers=arrJSON["markers"]
          arrResult["has_markers"]=True
          arrResult["markers"]=arrMarkers

        if arrResult["has_audio_file"]:
          self.LoadAudioWithoutPlaying(self.strAudioFilename)

        if arrResult["has_lyrics_file"]:
          self.LoadLyrics(self.GetLyricsFilename())

        if arrResult["has_markers"]:
          boolError=False
          if len(arrResult["markers"]["marker_time"])!=12 or len(arrResult["markers"]["marker_name"])!=12:
            boolError=True
            strError="Broken marker data in project file \""+strFilename+"\""
            MsgBox_UserError(strError)
          else:
            # Decode marker names
            for n in range(12):
              self.SetMarkerTime(n, int(arrResult["markers"]["marker_time"][n]))
              self.SetMarkerName(n, str(arrResult["markers"]["marker_name"][n]))
            self.RenderMarkers()
            #return arrResult
            SetWindowHeader(os.path.relpath(self.strProjectFilename))
            winUI.sldAudioPosition.setValue(0)
            MusicPause()


  #----------------------------------------------------------------------------


  def SaveToDisk(self, strFilename):
    if strFilename:
      self.strProjectFilename=strFilename

    strLyricsFile=self.GetLyricsFilename()
    if strLyricsFile=="":
      self.SetLyricsFilename(strFilename[:-7]+".txt")

    self.SaveLyrics()
    winUI.lbLyricsName.setText(self.GetLyricsFilenameNoPath())

    # Get markers from UI
    self.ReadMarkersFromGui()

    # Build arrProject
    arrProject={}
    arrProject["has_audio_file"]=False
    arrProject["has_lyrics_file"]=False
    arrProject["has_markers"]=False
    
    arrProject["audio_file"]=self.strAudioFilename
    if self.strAudioFilename:
      arrProject["has_audio_file"]=True

    arrMarkers={}
    arrMarkers["marker_time"]=[]
    arrMarkers["marker_name"]=[]
    for i in range(12):
      arrMarkers["marker_time"].append(self.GetMarkerTime(i))
      arrMarkers["marker_name"].append(self.GetMarkerName(i))
#    print "Times:"
#    print arrMarkers["marker_time"]
#    print
#    print "Names:"
#    print arrMarkers["marker_name"]

    arrProject["markers"]=arrMarkers
    arrProject["has_markers"]=True

    arrProject["lyrics_file"]=self.strLyricsFilename
    arrProject["has_lyrics_file"]=True

    #strJSON=json.dumps(arrProject, indent=2, separators=(",", ": "))
    #arrJSON=strJSON.split("\n")
    #json_file.write(unicode(arrJSON))
    #WriteTextFile(arrJSON, strFilename)

    #oInputFile=file()

    if boolUsePickleInsteadOfJson:
      #oPickledProject=pickle.dumps(arrProject)
      oBinaryFile=open(strFilename, mode="wb")
      oPickledProject=pickle.dump(arrProject, oBinaryFile)
      oBinaryFile.close()
    else:
      oOutputFile=codecs.open(strFilename, "w", "utf-8")
      json.dump(arrProject, oOutputFile, indent=2, separators=(",", ": "), sort_keys=True, ensure_ascii=False)

    SetWindowHeader(os.path.relpath(self.strProjectFilename))


  #----------------------------------------------------------------------------


## Project
###############################################################################################

## Classes
###############################################################################################
###############################################################################################


if __name__=="__main__":
  #############################################################################
  ## Test code

  if 1==2:
    print "file: "+__file__
    print "abspath(file): "+os.path.abspath(__file__)
    print "relpath(file): "+os.path.relpath(__file__)
    print "dirname(abspath(file)): "+os.path.dirname(os.path.abspath(__file__))
    print "basename(abspath(file)): "+os.path.basename(os.path.abspath(__file__))
    print

#    file: LyricsHelper.py
#    abspath(file): D:\Projekt\_Tools\LyricsHelper\LyricsHelper.py
#    relpath(file): LyricsHelper.py
#    dirname(abspath(file)): D:\Projekt\_Tools\LyricsHelper
#    basename(abspath(file)): LyricsHelper.py


    strCurrentPath=os.path.dirname(os.path.abspath(__file__))
    arrCurrentPath=strCurrentPath.split(os.sep)
    print "Current file: "+os.path.basename(strCurrentPath)
    print "Current path:"
    for this in arrCurrentPath:
      print " "*4+str(this)
    print

#    Current file: LyricsHelper
#    Current path:
#        D:
#        Projekt
#        _Tools
#        LyricsHelper

    strPath1=os.path.join("D:", "Projekt", "_Tools", "LyricsHelper")
    strPath2=os.path.join("D:", "Projekt", "meow")
    print "strPath1: "+strPath1
    print "strPath2: "+strPath2
    print "Relative path (2, 1): "+os.path.relpath(strPath2, strPath1)
    print "Relative path (1, 2): "+os.path.relpath(strPath1, strPath2)
    print
    
#    strPath1: D:Projekt\_Tools\LyricsHelper
#    strPath2: D:Projekt\meow
#    Relative path (2, 1): ..\..\meow
#    Relative path (1, 2): ..\_Tools\LyricsHelper
    
    strPath1=os.path.join("D:", "Projekt", "_Tools", "LyricsHelper", "myfile.bin")
    strPath2=os.path.join("D:", "Projekt", "meow", "anotherfile.bin")
    print "strPath1: "+strPath1
    print "strPath2: "+strPath2
    print "Relative path (2, 1): "+os.path.relpath(strPath2, strPath1)
    print "Relative path (1, 2): "+os.path.relpath(strPath1, strPath2)
    print

#    strPath1: D:Projekt\_Tools\LyricsHelper\myfile.bin
#    strPath2: D:Projekt\meow\anotherfile.bin
#    Relative path (2, 1): ..\..\..\meow\anotherfile.bin
#    Relative path (1, 2): ..\..\_Tools\LyricsHelper\myfile.bin
    

  if 1==2:
    arrMeow=[]
    arrMeow.append(123)
    arrMeow.append([456, 789])
    arrMeow.append("abc")
    arrMeow.append(["def", "ghi", "jkl"])
    arrMeow.append(1000)
    for this in arrMeow:
      print str(this)
    exit(0)

    arrA=[]
    for i in range(5):
      print "row "+str(i)
      arrA.append(None)
      arrA[i]=i
    print
    print str(arrA)
    exit(0)


  ## Test code
  #############################################################################

  
  if len(sys.argv)>=2:
    if sys.argv[1].lower()=="-dump":
      print "Not supported!"
      exit(-1)


  ###############################################################################################
  ## Project instance


  currentProject=cProject()


  ## Project instance
  ###############################################################################################
  ## pyglet


  pyglet.options['audio'] = ('directsound', 'openal', 'pulse', 'silent')


  ## pyglet
  ###############################################################################################
  ## Qt setup


  #thisApp=QtGui.QApplication(sys.argv)
  thisApp=cApp(sys.argv)
  winOptions=cWinMain()
  winUI=cWinMainUI()
  winUI.setupUi(winOptions)
  
  winOptions.show()

  SliderThread=QtCore.QThread()
  SliderThread.run=MusicUpdate
  SliderThread.start()
  

  ## Qt setup
  ###############################################################################################
  ## Test stuffs



  ## Test stuffs
  ###############################################################################################
  ## Go!


  sys.exit(thisApp.exec_())


  ## Go!
  ###############################################################################################










