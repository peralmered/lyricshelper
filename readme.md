# LyricsHelper

LyricsHelper is a program to help a songwriter while writing lyrics.

You load a .WAV file, and you can play it, scrub to any position, and place markers (that you can name) at up to 12 different positions in the song.

While editing the lyrics, you can jump to markers (F1-F12) and set/change markers (CTRL+F1-F12) without leaving the text editor box.

Lyrics can be loaded and saved as regular .txt files (utf-8 format). 


### Projects (.lhproj)

A project consists of all the markers, and the filenames for the audio and lyrics - not the actual audio data, and not the actual lyrics text.


## Notes

* Currently the only audio format supported is .WAV - this is a limitation of the pyglet library's dependence on AVBin (which I can't get working here).


## Requirements

* Python v2.7.x

* PySide

        pip install pyside

* pyglet

        pip install pyglet

* xia.py

    https://bitbucket.org/peralmered/xia_lib/src/master/


## Multiplatform support

As long as your O/S supports one of the following audio systems, LyricsHelper should work:

* DirectSound
* OpenAL
* Pulse


## Keyboard shortcuts

F1-F12: Jump to marker 1-12

CTRL+F1-F12: Set marker 1-12 to current song position

Esc: Play/Pause toggle
